package tasks.task10;
import java.util.Scanner;

public class Task10 { //Проверка строки на палиндром
    static Scanner read = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите слово/фразу для проверки -  ");
        String s = read.nextLine();
        s = s.replaceAll("[^A-Za-zА-Яа-я0-9]", "");
        if (s.toLowerCase().equals((new StringBuffer(s)).reverse().toString().toLowerCase())) {
            System.out.println("Палиндром");
        } else {
            System.out.println("Не палиндром");
        }
    }
}