package tasks.task3;
import java.util.Scanner;

public class Task3 { //Метод для перевода рублей в евро по заданному курсу
    public static void main (String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.print("Введите курс - ");
        double euro = read.nextDouble();
        System.out.print("Введите сумму для перевода в руб. - ");
        double rub = read.nextDouble();

        System.out.printf(rub + " руб. = %.2f евро", transfer(rub, euro));
    }

    public static double transfer(double rub, double euro) {
        double transfer = rub / euro;
        return transfer;
    }
}
