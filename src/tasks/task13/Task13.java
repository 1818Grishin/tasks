package tasks.task13;

import java.util.Scanner;

public class Task13 { //Определение типа вводимого символа
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.println("Введите символ -  ");
        String string = read.nextLine();
        if(Character.isDigit(string.charAt(0))){
            System.out.println("Число");
        }
        if(Character.isLetter(string.charAt(0))){
            System.out.println("Буква");
        }
        if(".,:;".contains(string)){
            System.out.println("Знак пункутуации");
        }
    }
}