package tasks.task6;
import java.util.Scanner;

public class Task6 { //Вывод таблицы умножения введённого числа
    public static void main (String[] args) {
        Scanner read = new Scanner(System.in);

        System.out.print("Введите число - ");
        int x = read.nextInt();

        for (int i = 1; i <= 10; i++) {
            System.out.println(x + "*" + i + " = " + x*i);
        }
    }
}
