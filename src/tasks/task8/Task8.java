package tasks.task8;
import java.util.Scanner;
public class Task8 { //Обнуление выбранного столбца
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите число: ");
        int num = scanner.nextInt()-1;
        int [][] matrix=new int [5][6];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print((matrix[i][j] = 1 + (int) (Math.random() * 10) )+ " ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
        nullification(num,matrix);
    }

    public static void nullification(int num, int [][]matrix){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if(j==num){
                    matrix[i][j] = 0;
                }
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}