package tasks.task2;
import java.util.Scanner;

public class Task2 { //Метод, увеличивающий выбранный элемент массива на 10%
    public static void main (String[] args) {
        Scanner read = new Scanner(System.in);
        int[] array = {10,20,30,40,50,60,70,80,90,100};

        System.out.print("Введите номер элемента - ");
        int x = read.nextInt() - 1;
        System.out.print("Число " + array[x] + ", увеличенное на 10% = " + percent(array, x));
    }

    public static double percent(int[] array, int x) {
        double percents = array[x] * 0.1 + array[x];
        return percents;
    }
}
