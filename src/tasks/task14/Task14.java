package tasks.task14;

public class Task14 { //Смена столбцов и строк местами
    public static void main (String[] args) {
        int[][] array = new int[3][3];

        for (int i = 0; i < array.length; i++) {
            System.out.println();
            for (int j = 0; j < array.length; j++) {
                array[i][j] = (int) ((Math.random() * 9));
                System.out.print(array[i][j] + " ");
            }
        }
        System.out.println("\n");

        for (int i = 0; i < 3; i++) {
            for (int j = i + 1; j < 3; j++) {
                int reversedArray = array[i][j];
                array[i][j] = array[j][i];
                array[j][i] = reversedArray;
            }
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
