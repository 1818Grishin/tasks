package tasks.task5;

public class Task5 { //Простые числа
    public static void main (String[] args) {
        for (int i = 2; i <= 100; i++) {
            int counter = 0;
            for (int x = 1; x <= i; x++){
                if (i % x == 0) {
                    counter++;
                }
            }

            if (counter <= 2) {
                System.out.println("Число " + i + " является простым");
            }

        }
    }
}
