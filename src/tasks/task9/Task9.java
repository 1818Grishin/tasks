package tasks.task9;
import java.util.Scanner;

public class Task9 { //Проверка числа на палиндром
    static Scanner read = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число - ");
        int s = read.nextInt();
        palindrome (s);
    }

    public static void palindrome (int s) {
        String str = String.valueOf(s);
        if (str.equals((new StringBuffer(str)).reverse().toString())) {
            System.out.println("Палиндром");
        } else {
            System.out.println("Не палиндром");
        }
    }
}
