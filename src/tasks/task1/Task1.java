package tasks.task1;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.println("Введите слово/фразу - ");
        String string = read.nextLine();
        int symbol=string.indexOf(".");
        System.out.println("Кол-во символов до точки - "+symbol);
        int space = 0;
        String[] symbols;
        symbols=string.split("");
        for(int i = 0;i<symbols.length;i++){
            if(symbols[i].equals(" ")){
                space++;
            }
        }
        System.out.println("Кол-во пробелов - " + space);
    }
}