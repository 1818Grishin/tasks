package tasks.task7;
import java.util.Scanner;

public class Task7 { //Является ли число типа double целым?
    public static void main (String[] args) {
        Scanner read = new Scanner(System.in);

        System.out.print("Введите число - ");
        double x = read.nextDouble();

        if (x % 1 == 0) {
            System.out.print("Число целое.");
        }
        else System.out.print("Число дробное.");
    }
}
