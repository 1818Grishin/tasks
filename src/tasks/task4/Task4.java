package tasks.task4;

public class Task4 { //Расчет расстояния до места удара молнии
    public static void main (String[] args) {
        double speed = 1234.8; //км\ч
        double interval = 6.8; //сек

        double result = speed / 3600 * interval;

        System.out.printf("Расстояние до места удара молнии - %.2f км", result);
    }
}
