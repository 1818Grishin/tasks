package tasks.task11;
import java.util.Scanner;

public class Task11 { //Сколько часов, минут и секунд в n-ном кол-ве суток
    public static void main (String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.print("Введите кол-во суток - ");
        int days = read.nextInt();

        int hours = days * 24;
        int minutes = hours * 60;
        int seconds = minutes * 60;

        System.out.println(days + " суток = " + hours + " час.; " + minutes + " мин.; " + seconds + " сек.");
    }
}
